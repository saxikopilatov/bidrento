<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201117182347 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX index_comments ON comments');
        $this->addSql('ALTER TABLE comments CHANGE is_hidden is_hidden TINYINT(1) DEFAULT NULL, CHANGE created_at created_at BIGINT DEFAULT NULL, CHANGE post post_id INT NOT NULL');
        $this->addSql('ALTER TABLE comments ADD CONSTRAINT FK_5F9E962A4B89032C FOREIGN KEY (post_id) REFERENCES posts (id)');
        $this->addSql('CREATE INDEX IDX_5F9E962A4B89032C ON comments (post_id)');
        $this->addSql('DROP INDEX index_posts ON posts');
        $this->addSql('ALTER TABLE posts CHANGE post post LONGTEXT NOT NULL, CHANGE created_at created_at BIGINT DEFAULT NULL, CHANGE edited_at edited_at BIGINT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comments DROP FOREIGN KEY FK_5F9E962A4B89032C');
        $this->addSql('DROP INDEX IDX_5F9E962A4B89032C ON comments');
        $this->addSql('ALTER TABLE comments CHANGE is_hidden is_hidden TINYINT(1) DEFAULT \'0\', CHANGE created_at created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, CHANGE post_id post INT NOT NULL');
        $this->addSql('CREATE INDEX index_comments ON comments (id, post)');
        $this->addSql('ALTER TABLE posts CHANGE post post TEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_general_ci`, CHANGE created_at created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, CHANGE edited_at edited_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
        $this->addSql('CREATE INDEX index_posts ON posts (id, title)');
    }
}
