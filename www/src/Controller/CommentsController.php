<?php

namespace App\Controller;

use App\Entity\Comments;
use App\Repository\CommentsRepository;
use App\Repository\PostsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class CommentsController extends AbstractController
{
	private $commentsRepository;
	private $postsRepository;
	private $security;

	public function __construct(Security $security, CommentsRepository $repCom, PostsRepository $repPosts)
	{
		$this->commentsRepository = $repCom;
		$this->postsRepository = $repPosts;
		$this->security = $security;
	}

	/**
	 * @Route("/comments/{postId}", name="comments", methods={"GET"})
	 * @param string $postId
	 * @return JsonResponse
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
    public function index($postId): JsonResponse
    {
	    $data = [
		    'errors' => [],
		    'comments' => null
	    ];

	    if (empty($postId))
	    {
		    $data['errors'] = [
			    'code' => 400,
			    'description' => 'Your post data is incorrect'
		    ];

		    return new JsonResponse($data);
	    }

	    $post = $this->postsRepository->getBySystemId($postId);
	    if (empty($post))
	    {
		    $data['errors'] = [
			    'code' => 400,
			    'description' => 'Your post data is incorrect'
		    ];

		    return new JsonResponse($data);
	    }

	    $comments = $post->getComments()->toArray();
	    $data['comments'] = array_map(function ($item) {
	    	/**
		     * @var Comments $item
		     */
	    	return $item->toArray();
	    }, $comments);

	    return new JsonResponse($data);
    }

	/**
	 * @Route("/comments/{postId}", name="commentadd", methods={"POST"})
	 * @param string $postId
	 * @param Request $request
	 * @return JsonResponse
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
	public function add($postId, Request $request): JsonResponse
	{
		$data = [
			'errors' => [],
			'comments' => null
		];

		if (empty($postId))
		{
			$data['errors'] = [
				'code' => 400,
				'description' => 'Your post data is incorrect'
			];

			return new JsonResponse($data);
		}

		$post = $this->postsRepository->getBySystemId($postId);
		if (empty($post))
		{
			$data['errors'] = [
				'code' => 400,
				'description' => 'Your post data is incorrect'
			];

			return new JsonResponse($data);
		}

		try {
			$input = json_decode($request->getContent(), true);
		} catch (\Exception $e) {
			$input = [];
		}

		if (empty($input['content']))
		{
			$data['errors'] = [
				'code' => 400,
				'description' => 'Your comment data is incorrect'
			];

			return new JsonResponse($data);
		}

		$comment = $this->commentsRepository->addComment($post, $input['content']);
		$data['comments'] = $comment->toArray();

		return new JsonResponse($data);
	}

	/**
	 * @Route("/comments/{id}", name="commentedit", methods={"PUT"})
	 * @param Request $request
	 * @return JsonResponse
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
	public function edit($id, Request $request): JsonResponse
	{
		$data = [
			'errors' => [],
			'comments' => null
		];

		if (!$this->security->getUser())
		{
			$data['errors'] = [
				'code' => 403,
				'description' => 'You are not allowed to add posts'
			];

			return new JsonResponse($data);
		}

		try {
			$input = json_decode($request->getContent(), true);
		} catch (\Exception $e) {
			$input = [];
		}

		if (empty($id))
		{
			$data['errors'] = [
				'code' => 400,
				'description' => 'Your post data is incorrect'
			];

			return new JsonResponse($data);
		}

		$comment = $this->commentsRepository->getBySystemId($id);
		if (empty($comment))
		{
			$data['errors'] = [
				'code' => 400,
				'description' => 'Your comment data is incorrect'
			];

			return new JsonResponse($data);
		}

		if (empty($input['content']))
		{
			$data['errors'] = [
				'code' => 400,
				'description' => 'Your comment data is incorrect'
			];

			return new JsonResponse($data);
		}

		$comment->setContent($input['content']);
		$comment = $this->commentsRepository->editComment($comment);
		$data['comments'] = $comment->toArray();

		return new JsonResponse($data);
	}

	/**
	 * @Route("/comments/{id}", name="commentdelete", methods={"DELETE"})
	 * @param string $id
	 * @return JsonResponse
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
	public function delete($id): JsonResponse
	{
		$data = [
			'errors' => [],
			'success' => null
		];

		if (!$this->security->getUser())
		{
			$data['errors'] = [
				'code' => 403,
				'description' => 'You are not allowed to add posts'
			];

			return new JsonResponse($data);
		}

		if (empty($id))
		{
			$data['errors'] = [
				'code' => 400,
				'description' => 'Your post data is incorrect'
			];

			return new JsonResponse($data);
		}

		$comment = $this->commentsRepository->getBySystemId($id);
		if (empty($comment))
		{
			$data['errors'] = [
				'code' => 400,
				'description' => 'Your comment data is incorrect'
			];

			return new JsonResponse($data);
		}

		$success = $this->commentsRepository->deleteComment($comment);
		$data['success'] = $success;

		return new JsonResponse($data);
	}

	/**
	 * @Route("/comments/{id}", name="commenthide", methods={"PATCH"})
	 * @param string $id
	 * @return JsonResponse
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
	public function hide($id): JsonResponse
	{
		$data = [
			'errors' => [],
			'comments' => null
		];

		if (!$this->security->getUser())
		{
			$data['errors'] = [
				'code' => 403,
				'description' => 'You are not allowed to add posts'
			];

			return new JsonResponse($data);
		}

		if (empty($id))
		{
			$data['errors'] = [
				'code' => 400,
				'description' => 'Your post data is incorrect'
			];

			return new JsonResponse($data);
		}

		$comment = $this->commentsRepository->getBySystemId($id);
		if (empty($comment))
		{
			$data['errors'] = [
				'code' => 400,
				'description' => 'Your comment data is incorrect'
			];

			return new JsonResponse($data);
		}

		$comment = $this->commentsRepository->hideComment($comment);
		$data['comments'] = $comment->toArray();

		return new JsonResponse($data);
	}
}
