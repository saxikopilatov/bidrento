<?php

namespace App\Controller;

use App\Entity\Posts;
use App\Repository\PostsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class PostsController extends AbstractController
{
	private $postsRepository;
	private $security;

	public function __construct(Security $security, PostsRepository $rep)
	{
		$this->postsRepository = $rep;
		$this->security = $security;
	}

    /**
     * @Route("/posts", name="posts", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function index(Request $request): JsonResponse
    {
        $lastId = null;
        if (!empty($request->query->get('last')))
        {
        	$lastPost = $this->postsRepository->getBySystemId($request->query->get('last'));
        	if (!empty($lastPost))
	        {
		        $lastId = $lastPost->getId();
	        }
        }

        $limit = null;
        if (!empty($request->query->get('limit')) && is_numeric($request->query->get('limit')))
        {
			if (!empty((int)$request->query->get('limit')))
			{
				$limit = (int)$request->query->get('limit');
			}
        }

        $data['posts'] = array_map(function ($item) {
	        /**
	         * @var Posts $item
	         */
	        return $item->toArray();
        }, $this->postsRepository->getPosts($limit, $lastId));
        $data['errors'] = [];

	    if ($this->security->getUser())
	    {
		    $data['actions'] = ['admin'];
	    }

        return new JsonResponse($data);
    }

	/**
	 * @Route("/posts/{id}", name="post", methods={"GET"})
	 * @param $id
	 * @return JsonResponse
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
    public function post($id): JsonResponse
    {
    	$post = $this->postsRepository->getBySystemId($id);
    	$data = [
    		'errors' => [],
		    'post' => null,
		    'actions' => []
	    ];

    	if (empty($post))
	    {
	    	$data['errors'] = [
	    		'code' => 404,
			    'description' => 'post not found'
		    ];
	    } else {
    		$data['post'] = $post->toArray();
	    }

	    if ($this->security->getUser())
	    {
		    $data['actions'] = ['admin'];
	    }

	    return new JsonResponse($data);
    }

	/**
	 * @Route("/posts", name="postadd", methods={"POST"})
	 * @param Request $request
	 * @return JsonResponse
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
    public function add(Request $request): JsonResponse
    {
	    $data = [
		    'errors' => [],
		    'post' => null
	    ];

    	if (!$this->security->getUser())
	    {
	    	$data['errors'] = [
			    'code' => 403,
			    'description' => 'You are not allowed to add posts'
		    ];

		    return new JsonResponse($data);
	    }

	    try {
		    $input = json_decode($request->getContent(), true);
	    } catch (\Exception $e) {
		    $input = [];
	    }

	    if (empty($input['title']) || empty($input['post']))
	    {
	    	$data['errors'] = [
			    'code' => 400,
			    'description' => 'Your post data is incorrect'
		    ];

		    return new JsonResponse($data);
	    }

		$post = $this->postsRepository->addPost($input['title'], $input['post']);
	    $data['post'] = $post->toArray();

	    return new JsonResponse($data);
    }

	/**
	 * @Route("/posts", name="postedit", methods={"PUT"})
	 * @param Request $request
	 * @return JsonResponse
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
    public function edit(Request $request): JsonResponse
    {
	    $data = [
		    'errors' => [],
		    'post' => null
	    ];

	    if (!$this->security->getUser())
	    {
		    $data['errors'] = [
			    'code' => 403,
			    'description' => 'You are not allowed to add posts'
		    ];

		    return new JsonResponse($data);
	    }

	    try {
		    $input = json_decode($request->getContent(), true);
	    } catch (\Exception $e) {
		    $input = [];
	    }

	    if (empty($input['id']))
	    {
		    $data['errors'] = [
			    'code' => 400,
			    'description' => 'Your post data is incorrect'
		    ];

		    return new JsonResponse($data);
	    }

	    $post = $this->postsRepository->getBySystemId($input['id']);
	    if (empty($post))
	    {
		    $data['errors'] = [
			    'code' => 400,
			    'description' => 'Your post data is incorrect'
		    ];

		    return new JsonResponse($data);
	    }

	    if (empty($input['title']) && empty($input['post']))
	    {
		    $data['errors'] = [
			    'code' => 400,
			    'description' => 'Your post data is incorrect'
		    ];

		    return new JsonResponse($data);
	    }

	    if (!empty($input['title'])) $post->setTitle($input['title']);
	    if (!empty($input['post'])) $post->setPost($input['post']);
		$post = $this->postsRepository->editPost($post);
	    $data['post'] = $post->toArray();

	    return new JsonResponse($data);
    }

	/**
	 * @Route("/posts/{id}", name="postdelete", methods={"DELETE"})
	 * @param string $id
	 * @return JsonResponse
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
    public function delete($id): JsonResponse
    {
	    $data = [
		    'errors' => [],
		    'success' => null
	    ];

	    if (!$this->security->getUser())
	    {
		    $data['errors'] = [
			    'code' => 403,
			    'description' => 'You are not allowed to add posts'
		    ];

		    return new JsonResponse($data);
	    }

	    $post = $this->postsRepository->getBySystemId($id);
	    if (empty($post))
	    {
		    $data['errors'] = [
			    'code' => 404,
			    'description' => 'post not found'
		    ];

		    return new JsonResponse($data);
	    }

	    $success = $this->postsRepository->deletePost($post);
	    $data['success'] = $success;

	    return new JsonResponse($data);
    }
}
