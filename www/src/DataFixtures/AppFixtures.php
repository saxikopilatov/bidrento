<?php

namespace App\DataFixtures;

use App\Entity\Comments;
use App\Entity\Posts;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 100; $i++)
        {
        	$post = new Posts();
			$post->setSystemId($faker->uuid);
			$post->setTitle($faker->realText(255));
			$post->setPost($faker->realText(10240));
			$manager->persist($post);

			for ($k = 0; $k < 2; $k++)
			{
				$comment = new Comments();
				$comment->setPost($post);
				$comment->setSystemId($faker->uuid);
				$comment->setContent($faker->realText(1023));
				$manager->persist($comment);
			}
        }

        $manager->flush();
    }
}
