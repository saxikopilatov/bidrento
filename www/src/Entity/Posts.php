<?php

namespace App\Entity;

use App\Repository\PostsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PostsRepository::class)
 */
class Posts
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
	 * @ORM\Column(type="string", length=36)
	 */
	private $system_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $post;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $edited_at;

    /**
     * @ORM\OneToMany(targetEntity=Comments::class, mappedBy="post")
     */
    private $comments;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
    }

    public function toArray(): array
    {
    	try {
    		$dateCreate = new \DateTime($this->getCreatedAt());
	    } catch (\Exception $e) {
    		$dateCreate = new \DateTime();
	    }

	    try {
		    $dateEdit = new \DateTime($this->getEditedAt());
	    } catch (\Exception $e) {
		    $dateEdit = new \DateTime();
	    }

	    return [
	    	'id' => $this->getSystemId(),
		    'title' => $this->getTitle(),
		    'post' => $this->getPost(),
		    'date_create' => $dateCreate->format('Y-m-d H:i:s'),
		    'date_edit' => $dateEdit->format('Y-m-d H:i:s'),

	    ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSystemId(): ?string
    {
    	return $this->system_id;
    }

    public function setSystemId(string $id): self
    {
    	$this->system_id = $id;

    	return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPost(): ?string
    {
        return $this->post;
    }

    public function setPost(string $post): self
    {
        $this->post = $post;

        return $this;
    }

    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }

    public function setCreatedAt(?string $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getEditedAt(): ?string
    {
        return $this->edited_at;
    }

    public function setEditedAt(?string $edited_at): self
    {
        $this->edited_at = $edited_at;

        return $this;
    }

    /**
     * @return Collection|Comments[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comments $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setPostId($this);
        }

        return $this;
    }

    public function removeComment(Comments $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getPostId() === $this) {
                $comment->setPostId(null);
            }
        }

        return $this;
    }
}
