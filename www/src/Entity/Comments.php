<?php

namespace App\Entity;

use App\Repository\CommentsRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Posts;

/**
 * @ORM\Entity(repositoryClass=CommentsRepository::class)
 */
class Comments
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
	 * @ORM\Column(type="string", length=36)
	 */
	private $system_id;

    /**
     * @ORM\ManyToOne(targetEntity=Posts::class, inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $post;

    /**
     * @ORM\Column(type="string", length=1023)
     */
    private $content;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_hidden;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $created_at;

    public function toArray(): array
    {
    	try {
		    $dateCreate = new \DateTime($this->getCreatedAt());
	    } catch (\Exception $e) {
    		$dateCreate = new \DateTime();
	    }

    	return [
    		'id' => $this->getSystemId(),
		    'content' => $this->getContent(),
		    'date_create' => $dateCreate->format('Y-m-d H:i:s'),
		    'hidden' => $this->getIsHidden()
	    ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

	public function getSystemId(): ?string
	{
		return $this->system_id;
	}

	public function setSystemId(string $id): self
	{
		$this->system_id = $id;

		return $this;
	}

    public function getPost(): ?Posts
    {
        return $this->post;
    }

    public function setPost(?Posts $post): self
    {
        $this->post = $post;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getIsHidden(): ?bool
    {
        return $this->is_hidden;
    }

    public function setIsHidden(?bool $is_hidden): self
    {
        $this->is_hidden = $is_hidden;

        return $this;
    }

    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }

    public function setCreatedAt(?string $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }
}
