<?php

namespace App\Repository;

use App\Entity\Comments;
use App\Entity\Posts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Ramsey\Uuid\Uuid;

/**
 * @method Comments|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comments|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comments[]    findAll()
 * @method Comments[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentsRepository extends ServiceEntityRepository
{
	private $manager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Comments::class);
	    $this->manager = $manager;
    }

	/**
	 * @param string $systemId
	 * @return Comments|null
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
	public function getBySystemId(string $systemId): ?Comments
	{
		return $this->createQueryBuilder('comment')
			->where('comment.system_id = :val')
			->setParameter('val', $systemId)
			->getQuery()
			->getOneOrNullResult();
	}

	/**
	 * @param Posts $post
	 * @param string $content
	 * @return Comments
	 */
	public function addComment(Posts $post, string $content): Comments
	{
		$uuid = Uuid::uuid4();

		$comment = new Comments();
		$comment->setPost($post)
			->setContent($content)
			->setSystemId($uuid->toString());

		$this->manager->persist($comment);
		$this->manager->flush();

		return $comment;
	}

	/**
	 * @param Comments $comment
	 * @return Comments
	 */
	public function editComment(Comments $comment): Comments
	{
		$this->manager->persist($comment);
		$this->manager->flush();

		return $comment;
	}

	/**
	 * @param Comments $comment
	 * @return bool
	 */
	public function deleteComment(Comments $comment): bool
	{
		try {
			$this->manager->remove($comment);
			$this->manager->flush();

			return true;
		} catch (\Exception $e) {
			return false;
		}
	}

	/**
	 * @param Comments $comment
	 * @return Comments
	 */
	public function hideComment(Comments $comment): Comments
	{
		$comment->setIsHidden(true);

		$this->manager->persist($comment);
		$this->manager->flush();

		return $comment;
	}

    // /**
    //  * @return Comments[] Returns an array of Comments objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Comments
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
