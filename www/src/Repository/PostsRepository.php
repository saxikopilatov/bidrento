<?php

namespace App\Repository;

use App\Entity\Posts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Ramsey\Uuid\Uuid;

/**
 * @method Posts|null find($id, $lockMode = null, $lockVersion = null)
 * @method Posts|null findOneBy(array $criteria, array $orderBy = null)
 * @method Posts[]    findAll()
 * @method Posts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostsRepository extends ServiceEntityRepository
{
	private $manager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Posts::class);
        $this->manager = $manager;
    }

	/**
	 * @param string $systemId
	 * @return Posts|null
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
    public function getBySystemId(string $systemId): ?Posts
    {
    	return $this->createQueryBuilder('post')
		    ->where('post.system_id = :val')
		    ->setParameter('val', $systemId)
		    ->getQuery()
		    ->getOneOrNullResult();
    }

	/**
	 * @param int|null $limit
	 * @param int|null $lastId
	 * @return array
	 */
    public function getPosts(?int $limit = null, int $lastId = null): array
    {
    	$query = $this->createQueryBuilder('post')
		    ->orderBy('post.id', 'DESC')
		    ->setMaxResults($limit);

    	if (!empty($lastId))
	    {
	    	$query->where('post.id < :val')
			    ->setParameter('val', $lastId);
	    }

    	return $query->getQuery()
		    ->getResult();
    }

	/**
	 * @param string $title
	 * @param string $content
	 * @return Posts
	 */
    public function addPost(string $title, string $content): Posts
    {
	    $uuid = Uuid::uuid4();

    	$post = new Posts();
    	$post->setTitle($title)
		    ->setPost($content)
	        ->setSystemId($uuid->toString());

    	$this->manager->persist($post);
    	$this->manager->flush();

    	return $post;
    }

	/**
	 * @param Posts $post
	 * @return Posts
	 */
    public function editPost(Posts $post): Posts
    {
	    $this->manager->persist($post);
	    $this->manager->flush();

	    return $post;
    }

	/**
	 * @param Posts $post
	 * @return bool
	 */
    public function deletePost(Posts $post): bool
    {
    	try {
    		$this->manager->remove($post);
    		$this->manager->flush();

    		return true;
	    } catch (\Exception $e) {
    		return false;
	    }
    }

    // /**
    //  * @return Post[] Returns an array of Post objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Post
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
