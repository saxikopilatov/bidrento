import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueCookies from 'vue-cookies'
import AxiosPlugin from 'vue-axios-cors'
import App from './App.vue'
import router from './router'

Vue.use(VueAxios, axios)
Vue.use(VueCookies)
Vue.use(AxiosPlugin)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
