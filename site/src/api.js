import Vue from 'vue';

const base = 'http://localhost:81/api/';
const makeUrl = (url, urlParams = {}) => {
	const paramsArr = [];
	if (Object.keys(urlParams).length > 0) {
		for (const key of Object.keys(urlParams)) {
			const value = urlParams[key];
			if (value) {
				paramsArr.push(`${key}=${value}`);
			}
		}
	}
	url += '?' + paramsArr.join('&');

	return base + url;
};

const apiGet = async (url, urlParams = {}) => {
	const response = await Vue.axios.get(makeUrl(url, urlParams), {withCredentials: true});
	return response.data;
};

const apiDelete = async (url, urlParams = {}) => {
	const response = await Vue.axios.delete(makeUrl(url, urlParams), {withCredentials: true});
	return response.data;
};

const apiPost = async (url, data, urlParams = {}) => {
	const response = await Vue.axios.post(makeUrl(url, urlParams), data, {withCredentials: true});
	return response.data;
};

const apiPatch = async (url, data, urlParams = {}) => {
	const response = await Vue.axios.patch(makeUrl(url, urlParams), data, {withCredentials: true});
	return response.data;
};

const apiPut = async (url, data, urlParams = {}) => {
	const response = await Vue.axios.put(makeUrl(url, urlParams), data, {withCredentials: true});
	return response.data;
};

export { apiGet, apiPost, apiPut, apiPatch, apiDelete };