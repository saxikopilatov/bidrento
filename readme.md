0. Copy content from `example.env` to `.env`
1. docker-compose up -d
2. docker exec -it bidrento_php bash
3. cd ..
4. bin/console doctrine:migrations:migrate
5. bin/console doctrine:fixtures:load
6. exit
7. cd site
8. npm i
9. npm run serve
10. Visit http://localhost:8080
