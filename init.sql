CREATE TABLE `posts` (`id` INT NOT NULL AUTO_INCREMENT,`system_id` varchar(36) NOT NULL,`title` varchar(255) NOT NULL,`post` text,`created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,`edited_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (id));
CREATE TABLE `comments` (`id` INT NOT NULL AUTO_INCREMENT,`system_id` varchar(36) NOT NULL,`post` INT NOT NULL,`content` varchar(1023) NOT NULL,`is_hidden` BOOLEAN DEFAULT false,`created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (id));
CREATE INDEX index_posts ON posts (id, title);
CREATE INDEX index_comments ON comments (id, post);
ALTER TABLE comments ADD CONSTRAINT fk_comments_posts FOREIGN KEY (post_id) REFERENCES posts (id);
